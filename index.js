const Koa = require('koa');
const app = new Koa();
const { getCounter } = require('./business/counter-business')

// response
app.use(ctx => {
  ctx.body = getCounter();
});

console.log("Listening!");
app.listen(5000);

